module HSplay
    ( 
    ) where


import Data.List (foldl')


{-======== Private Interface ========-}
data Node a
    = Empty
    | Node a (Node a) (Node a)
    deriving (Show)


-- Use a zipper for supporting traversal, which is needed when splaying
data Crumb a
    = LeftCrumb a (Node a)
    | RightCrumb a (Node a)
    deriving (Show)


type Crumbs a = [Crumb a]
type Zipper a = (Node a, Crumbs a)


{-------- Traversal --------}

goLeft :: (Zipper a) -> Maybe (Zipper a)
goLeft (Empty, _) = Nothing
goLeft (Node value left right, crumbs) =
    Just (left, (LeftCrumb value right):crumbs)


goRight :: (Zipper a) -> Maybe (Zipper a)
goRight (Empty, _) = Nothing
goRight (Node value left right, crumbs) =
    Just (right, (RightCrumb value left):crumbs)


goUp :: (Zipper a) -> Maybe (Zipper a)
goUp (_ , []) = Nothing
goUp (curNode, (LeftCrumb value right):crumbs) =
    Just (Node value curNode right, crumbs)
goUp (curNode, (RightCrumb value left):crumbs) =
    Just (Node value left curNode, crumbs)


{-------- Splaying --------}

splay :: (Zipper a) -> (Tree a) -> (Tree a)
splay (Node value left right, crumbs) tree = tree
-- TODO

{-------- Insertion --------}

binaryInsert :: (Ord a) => a -> (Zipper a) -> (Zipper a)

-- Insertion into an empty node simply replaces the empty node with the
-- new node
binaryInsert newValue (Empty, crumbs) = (Node newValue Empty Empty, crumbs)

-- TODO: See if this can be simplified with a fold
binaryInsert newValue (Node value left right, crumbs)
    | newValue < value = insertLeft
    | otherwise = insertRight
    where
        (newLeft, _) = binaryInsert newValue ( left
                                             , (LeftCrumb value right):crumbs
                                             )
        (newRight, _) = binaryInsert newValue ( right
                                              , (RightCrumb value left):crumbs
                                              )
        insertLeft = (Node value newLeft right, crumbs) 
        insertRight = (Node value left newRight, crumbs)


{- ======== Public Interface ======== -}

-- Keep a separate tree structure so that we can maintain metadata for the
-- tree, such as its size
data Tree a
    = Tree { root :: (Node a)
           , size :: Int
           }
    deriving(Show)


emptyTree :: Tree a
emptyTree = Tree Empty 0


insert :: (Ord a) => a -> (Tree a) -> (Tree a)
insert newValue tree =
    -- TODO: binaryInsert needs to return a zipper to the inserted node,
    -- not the zipper to newRoot, which is []. Then splay can take that
    -- zipper, as well as the newRoot, and return another newRoot', which
    -- is the result of the splay operation
    let
        (newRoot, _) = binaryInsert newValue (root tree, [])
    in tree { root = newRoot
            , size = (size tree) + 1
            }


fromList :: (Ord a) => [a] -> Tree a
fromList = foldl' (flip insert) emptyTree
--fromList = foldr insert emptyTree
